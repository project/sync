<?php

/**
 * @file
 * Administrative page callbacks for the sync module.
 */

/**
 * Synchronisation basic settings
 */
function sync_settings_form() {

  $form['incomming'] = array(
    '#type' => 'fieldset',
    '#description' => t('Configure settings for incomming sync requests'),
    '#title' => 'Incomming',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['incomming']['sync_local_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t('Local user'),
    '#default_value' => variable_get('sync_local_username', false),
  );
  $form['incomming']['sync_incomming_timing'] = array(
    '#type' => 'selectbox',
    '#title' => 'Timing',
    '#description' => t('Select when the sync has to be done'),
    '#options' => array( 0 => 'On approval', 1 => 'Next cron after approval',  2 => 'On creation', 3 => 'Next cron after creation'),
    '#default_value' => variable_get('sync_incomming_timing', 0),
  );

  $form['outgoing'] = array(
    '#type' => 'fieldset',
    '#description' => t('Configure settings for outgoing sync requests'),
    '#title' => 'Outgoing',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['outgoing']['sync_destination'] = array(
    '#type' => 'textfield',
    '#title' => t('Sync destination'),
    '#default_value' => variable_get('sync_destination', false),
  );
  $form['outgoing']['sync_remote_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t('Remote user'),
    '#default_value' => variable_get('sync_remote_username', false),
  );
  $form['outgoing']['sync_remote_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#description' => t('Password of remote user'),
    '#default_value' => variable_get('sync_remote_password', false),
  );
  $form['outgoing']['sync_timing'] = array(
    '#type' => 'selectbox',
    '#title' => 'Timing',
    '#description' => t('Select when the sync has to be done'),
    '#options' => array( 0 => 'On approval', 1 => 'Next cron after approval',  2 => 'On creation', 3 => 'Next cron after creation'),
    '#default_value' => variable_get('sync_outgoing_timing', 0),
  );
  
  return system_settings_form($form);
}

?>